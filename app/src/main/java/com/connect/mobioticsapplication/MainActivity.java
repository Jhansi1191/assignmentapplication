package com.connect.mobioticsapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button encrypt,decrypt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        encrypt = (Button)findViewById(R.id.encrypt);
        decrypt = (Button)findViewById(R.id.decrypt);

        encrypt.setOnClickListener(this);
        decrypt.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this,SecondActivity.class);
        switch (view.getId()){
            case R.id.encrypt:
                intent.putExtra("TYPE","encrypt");
                startActivity(intent);
                break;
            case R.id.decrypt:
                intent.putExtra("TYPE","decrypt");
                startActivity(intent);
                break;
        }
    }
}
