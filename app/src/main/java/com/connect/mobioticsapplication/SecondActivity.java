package com.connect.mobioticsapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    EditText editText;
    Button submit;
    TextView result;
    String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        editText = (EditText) findViewById(R.id.editText);
        submit = (Button) findViewById(R.id.submit);
        result = (TextView) findViewById(R.id.textView);

        if (getIntent() != null) {
            type = getIntent().getStringExtra("TYPE");
            if (type.equals("encrypt")) {
                editText.setHint("Enter encrypt message");
            } else {
                editText.setHint("Enter decrypt message");
            }
        }

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                result.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editText.getText().toString().length() > 0) {
                    submit.setEnabled(true);
                    submit.setTextColor(getColor(android.R.color.white));
                } else {
                    result.setVisibility(View.INVISIBLE);
                    submit.setTextColor(getColor(android.R.color.black));
                    submit.setEnabled(false);
                }
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals("encrypt")) {
                    encryptMessage(editText.getText().toString());
                } else {
                    decryptMessage(editText.getText().toString());
                }
            }
        });

    }

    /***
     *
     *  Encrypt a message
     *
     * @param message
     */

    private void encryptMessage(String message) {
        if (message != null) {
            String encryptedmsg = "";
            char[] chars = message.toCharArray();

            int count = 1;
            char prev = '\0';
            int k = 0;

            for (int i = 0; i < chars.length; i++) {
                if (prev != chars[i]) {
                    chars[k++] = chars[i];
                    prev = chars[i];
                    count = 1;
                } else {
                    count = (count + 1);
                    encryptedmsg = encryptedmsg.substring(0, encryptedmsg.length() - 2);
                }
                encryptedmsg += (chars[i] + "" + count);
            }
            result.setVisibility(View.VISIBLE);
            result.setText(encryptedmsg);
        }
    }

    /***
     * Decrypt an encrypted message
     * @param encryptedmsg
     */
    private void decryptMessage(String encryptedmsg) {
        if (encryptedmsg != null) {
            String decryptedmsg = "";
            char[] chars = encryptedmsg.toCharArray();


            char prev = '\0';
            int k = 0;

            for (int i = 0; i < chars.length; i++) {
                if (prev != chars[i] && !Character.isDigit(chars[i])) {
                    chars[k++] = chars[i];
                    prev = chars[i];
                } else {
                    if (Character.isDigit(chars[i]) && (Character.getNumericValue(chars[i]) > 1)) {
                        decryptedmsg += (prev);
                        continue;
                    } else {
                        continue;
                    }
                }
                decryptedmsg += (chars[i]);
            }
            result.setVisibility(View.VISIBLE);
            result.setText(decryptedmsg);
        }
    }
}
